package com.example.lesson9;

import com.example.lesson9.MainActivity;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	TextView textview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview = (TextView) findViewById(R.id.textview);
		String name = getPreferences(MODE_PRIVATE).getString("name", "");
		if (name == "")
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			final EditText input = new EditText(MainActivity.this);
			builder.setMessage("Please enter name");
			builder.setView(input);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){					
				@Override
				public void onClick(DialogInterface dialog, int which) {
					final SharedPreferences prefs = (SharedPreferences) getPreferences(MODE_PRIVATE);
					final SharedPreferences.Editor editor = prefs.edit();
					editor.putString("name", input.getText().toString());
					editor.commit();
					
					String newname = getPreferences(MODE_PRIVATE).getString("name", "");
					textview.setText(newname);
				}});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
	
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub						
				}
				
			});
			builder.show();
			
		}
		else
		{
			textview.setText(name);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public void logout(View view)
	{
		final SharedPreferences prefs = (SharedPreferences) getPreferences(MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.clear();
		editor.commit();
		finish();
	}

}
